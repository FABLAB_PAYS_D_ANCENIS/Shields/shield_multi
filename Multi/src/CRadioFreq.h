#ifndef CRADIOFREQ_H
#define CRADIOFREQ_H

#include "generic.h"
#include "NRFLite.h"

extern char m_bRadioFreqUSed;

struct RadioPacket
{
    int32_t  s32Data;
    uint16_t u16Identifier;
    uint8_t  u8Emitter;
};



class CRadioFreq
{
public:
    CRadioFreq(uint8 u8PinCE, uint8 u8PinCSN);
    void init(uint8 u8Id);
    void destAddress(uint8 u8Id);
    void stop(void);
    bool isCodeReceived(void);
    int32 receiveCode(void);
	  int16 receiveAddr(void);
    int32 receiveType(void);
    void send (int32 s32Data, uint16 u16Id);

private:
	uint8 m_u8PinCE;
	uint8 m_u8PinCSN;
    NRFLite *_radio;
    RadioPacket _radioData;
    uint8_t  u8AddrEmitter;
    uint8_t  u8AddrDest;
    bool bIsInitialized;
    int32 s32CodeReceived;
    int16 u8AddrReceived;
    uint16 u16TypeReceived;
};

#endif // CRADIOFREQ_H
