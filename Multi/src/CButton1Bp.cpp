#include "CButton1Bp.h"

/*****************************************************************************
 * Function    : CButton1Bp                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CButton1Bp::CButton1Bp()
{
    // Setup pin
	m_u8PinButton = 0;
    pinMode(m_u8PinButton, INPUT_PULLUP);

    // Init tempos
    u32TempoBpOk   = 0;
    u32TempoBpOkEdge   = 0;

    // Init states
    bBpOkstate = fabDigitalRead();

	  // Init old state
    bBpOkOldState   = 0;
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CButton1Bp::getEdgeValue(void)
{
	bool bRes = 0;

	if((millis() - u32TempoBpOkEdge) > 100)
	{
		bRes = fabDigitalRead();

		if(bBpOkOldState != bRes)
		{
			u32TempoBpOkEdge = millis();

			if(bRes > bBpOkOldState)
			{
				bBpOkOldState = bRes;
				return 1;
			}
		}

		bBpOkOldState = bRes;

		return 0;
	}
	else
	{
		return 0;
	}

    return bRes;
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CButton1Bp::getValue(void)
{
    bool bRes = 0;

	if((millis() - u32TempoBpOk) > 100)
	{

		if(bBpOkstate != fabDigitalRead())
		{
			bBpOkstate = fabDigitalRead();
			u32TempoBpOk = millis();
		}

		return bBpOkstate;
	}
	else
	{
		return bBpOkstate;
	}

    return bRes;
}

/*****************************************************************************
 * Function    : fabDigitalRead                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CButton1Bp::fabDigitalRead(void)
{

    return digitalRead(m_u8PinButton) == 0;
}
