#ifndef CSYSTEMMULTI_H
#define CSYSTEMMULTI_H

#include "generic.h"

class CSystemMulti
{
public:
    CSystemMulti();
    void configure(void);
    void check(void);
    void disableErrors(void);
	void error(uint8 u8Err);

private:
    bool bErrorDisabled;
};

#endif // CSYSTEMMULTI_H
