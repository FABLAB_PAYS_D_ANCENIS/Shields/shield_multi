#include <SPI.h>
#include <SoftwareSerial.h>
#include "generic.h"
#include "CRGBLedShield.h"
#include "CButton1Bp.h"
#include "CRadioFreq.h"
#include "CSystemMulti.h"
//#include "CWifi.h"


// Classes
CRgbLedShield  *RgbLed;
CButton1Bp     *Button;
CRadioFreq     *RadioFreq;
CSystemMulti   *System;
//CWifi          *Wifi;

// Variables
uint32 u32Count;
uint32 u32Tempo;
uint32 u32Tmp;
uint32 u32Counter;
bool   bOldBpUp;
bool   bOldBpDown;
bool   bOldBpRight;
bool   bOldBpLeft;
uint8  u8BpType;
uint8  u8OldBpType;
uint8  u8MainStep;
uint8  u8AuxStep;
uint8  u8ColorR, u8ColorG, u8ColorB;

// Enums
enum {cStepLedRgb=0,
      cStepBp,
      cStepRfTx,
      cStepRfRx,
      cStepWifi
      };

/*****************************************************************************
 * Function    : setup                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void setup()
{
    // Serial
    Serial.begin(9600);

    // Instanciate classes
    System      = new CSystemMulti();
    RgbLed      = new CRgbLedShield(4);
    Button      = new CButton1Bp();
    RadioFreq   = new CRadioFreq(9,7);
    //Wifi        = new CWifi(10,8);

    System->configure();
    System->disableErrors();

    // Init variables
    u32Count   = 0;
    bOldBpUp   = 0;
    u8MainStep = cStepLedRgb;
    u8AuxStep  = 0;
    u32Counter = 0;
    u32Tempo   = millis();
    u8OldBpType = 0;        
}

/*****************************************************************************
 * Function    : loop                                                        *
 *...........................................................................*
 * Description : The loop function runs over and over again forever          *
 *****************************************************************************/
void loop()
{
    int32  s32Tmp;
    uint16 u16Tmp;
    uint32 u32Tmp;

    System->check();

    // Step machine
    switch(u8MainStep)
    {
        /* ------------------------  Leds RGB -----------------------------*/
        case cStepLedRgb:

            
            if((millis() - u32Tempo) > 50)
            {
                RgbLed->switchOnLedRgb(u8ColorR, u8ColorG, u8ColorB);

                if(u8AuxStep < 9)
                {
                   u8AuxStep++;
                }
                else
                {
                   u8AuxStep = 0;
                   u8ColorR  = random(0,2)*10;
                   u8ColorG  = random(0,2)*10;
                   u8ColorB  = random(0,2)*10;

                   while(u8ColorR == 0 && u8ColorG == 0 && u8ColorB == 0)
                   {
                       u8ColorR  = random(0,2)*10;
                       u8ColorG  = random(0,2)*10;
                       u8ColorB  = random(0,2)*10;
                   }
                }

                u32Tempo = millis();
            }
        break;

        /* -----------------------  Buttons ------------------------------*/
        case cStepBp:

            
            if(Button->getValue() == 1)
            {
                u8BpType = 1;

            }

            u8OldBpType = u8BpType;
            

        break;

        /* ------------------  Radio-frequency ---------------------------*/
        case cStepRfTx:
            if(u8AuxStep == 0)
            {

                if((millis() - u32Tempo) > 700)
                {
                    u8AuxStep = 1;
                    RadioFreq->init(1);
                    RadioFreq->destAddress(0);

                    u16Tmp = random(1000,9999);

                    RadioFreq->send(u16Tmp, 1);
                    u32Tempo = millis();
                }
            }
            else
            {
                if((millis() - u32Tempo) > 3000)
                {
                    u16Tmp = random(1000,9999);

                    RadioFreq->send(u16Tmp, 0);
                    u32Tempo = millis();
                }
            }

        break;

        /* ------------------  Radio-frequency ---------------------------*/
        case cStepRfRx:
            if(u8AuxStep == 0)
            {
                
                if((millis() - u32Tempo) > 700)
                {
                  u8AuxStep = 1;

                  RadioFreq->init(0);
                  RadioFreq->destAddress(1);
                }
            }
            else
            {
                if((millis() - u32Tempo) > 500)
                {
                    s32Tmp = RadioFreq->receiveCode();
                    if(s32Tmp >= 0)
                    {

                    }
                    u32Tempo = millis();
                }
            }

        break;

        /* ------------------  Wifi ---------------------------*/
        case cStepWifi:
            if(u8AuxStep == 0)
            {
                u8AuxStep = 1;
              

                //Wifi->connect((char*)("Fablab"), (char*)("Fablab44"));


                //Wifi->loginFree((char*)(" "), (char*)(" "));
            }
            else
            {
                if(Button->getValue() == 1)
                {
                   u8AuxStep++;
                   //Wifi->send(u8AuxStep);
                   //Wifi->sendSms((char*)("Hello !"));
                }
             }
        break;

        /* ------------------------- Default ----------------------------*/
        default:
            // Init variables
            u32Count = 0;
            bOldBpUp = 0;
            u8MainStep = cStepLedRgb;
            u8AuxStep = 0;
            u32Tmp = 0;
            u32Tempo = millis();
            RadioFreq->stop();
        break;
    }

    /* ---------------- Increment step machine ------------------------*/
    if((Button->getValue() == 1) && (bOldBpUp == 0))
    {
        u8MainStep++;
        u32Tempo = millis();
        u32Tmp = 0;
        u8AuxStep = 0;

        // Stop all devices
        RgbLed->switchOffLed();
        
    }
    bOldBpUp    = Button->getValue();
}
